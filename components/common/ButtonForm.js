import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import {APP_COLORS} from '../../style/color';
import PropTypes from 'prop-types';

const ButtonForm = ({ onPress, children, disabled }) => {
	const { buttonStyle, textStyle, buttonDisabled, buttonEnabled} = styles;

	return (
		<TouchableOpacity onPress={onPress} 
			style={ (disabled) ? [buttonStyle,buttonDisabled] : [buttonStyle,buttonEnabled]} disabled={disabled}>
			<Text style={ (disabled) ? [textStyle,{color: APP_COLORS.secondaryText || 'gray'}] : [textStyle, {color: APP_COLORS.primary || 'white', borderColor: APP_COLORS.primary || 'white'}]} >
				{children}
			</Text>
		</TouchableOpacity>
	);
};

ButtonForm.propTypes = {
	onPress: PropTypes.func,
	children: PropTypes.string,
	disabled: PropTypes.bool
};

const styles = {
	textStyle: {
		alignSelf: 'center',
		fontSize: 16 ,    
		fontWeight: '600',
		paddingTop: 10,
		paddingBottom: 10
	},
	buttonStyle: {
		flex: 1,
		alignSelf: 'stretch',
		borderRadius: 5,
		borderWidth: 1,
  
	},
	buttonDisabled: {
		backgroundColor: APP_COLORS.primary || 'white',
		borderColor: APP_COLORS.secondaryText || 'gray',
	},
	buttonEnabled: {
		backgroundColor: APP_COLORS.primaryAction || 'red',
		borderColor: APP_COLORS.primary || 'white',
	}
};

export {ButtonForm};


