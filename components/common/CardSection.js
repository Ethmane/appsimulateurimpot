import React from 'react';
import { View } from 'react-native';
import {APP_COLORS} from '../../style/color';
import PropTypes from 'prop-types';

const CardSection = ({children, last}) => {
	return (
		<View style={ (last) ? [styles.containerStyle, {paddingBottom: 10}] : [styles.containerStyle] }>
			{children} 
		</View>
	);
};

CardSection.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.node,
		PropTypes.string
	]),
	last: PropTypes.bool,
};

const styles = {
	containerStyle: {
		paddingHorizontal: 10,
		paddingTop: 10,
		justifyContent: 'flex-start',
		flexDirection: 'row',
		borderColor: APP_COLORS.lines || 'gray',
		position: 'relative',
	}
};

export {CardSection};


