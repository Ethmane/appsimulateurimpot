
import React from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import PropTypes from 'prop-types';

const Input = ({onChangeText, value, placeholder}) => {
	return (
		<View style={styles.inputContainer}>
			<TextInput
				multiline
				style={styles.input}
				placeholder={placeholder}
				value={value}
				autoFocus={true}
				underlineColorAndroid='transparent'
				onChangeText={(text) => onChangeText(text)}
			/>
		</View>
          
	);
};

Input.propTypes = {
	onChangeText: PropTypes.func.isRequired,
	value: PropTypes.number,
	placeholder: PropTypes.string
};

const styles = StyleSheet.create({
	inputContainer: {
		borderRadius: 20,
		borderWidth: 0.5, 
		borderColor: 'gray',
		backgroundColor: 'white'
	},
	input: {
		fontSize: 16,
		paddingLeft: 15
	},
});

export  {Input};
